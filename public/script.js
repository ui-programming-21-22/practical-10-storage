const canvas = document.getElementById("the_canvas")
const context = canvas.getContext("2d");

let image = new Image();
image.src = "calcifer.jpg";
let image2 = new Image();
image2.src = "spritesheet.png";

image.onload = gameloop;

// Nipplejs manager
var manager = nipplejs.create({
    color: 'blue'
    });

manager.on('added', function (evt, nipple) 
{
   nipple.on('dir:up', function (evt, data)
   {
        console.log("help");
   });
});


// GameObject holds positional information
// Can be used to hold other information based on requirements
function GameObject(spritesheet, x, y, width, height, scale) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.scale = scale;
}

// Default Player
let player = new GameObject(image2, 300, 100, 44, 50, 2);
let player2 = new GameObject(image2, 500, 100, 44, 50, 2);
let player3 = new GameObject(image2, 800, 100, 46, 34, 2);

// Setting Health
let playerHealth = 100;
// Setting Score
let playerScore = localStorage.getItem("score");
if (playerScore == null)
{
    playerScore = 0;
}

// The GamerInput is an Object that holds the Current
// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input


function input(event) {

    if (event.type === "keydown") 
    {
        switch (event.keyCode) 
        {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break;
            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break; 
            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break;
            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break;
            case 69: // E Key
                gamerInput = new GamerInput("E");
                break;
            case 81: // Q Key
                gamerInput = new GamerInput("Q");
                break;
            case 32: // Q Key
                gamerInput = new GamerInput("Space");
                break;
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } 
    else 
    {
        gamerInput = new GamerInput("None");
    }

    //Key Release Events
    if (event.type === "keyup") 
    {
        switch (event.keyCode) 
        {
            case 37: // Left Arrow
                gamerInput = new GamerInput("LeftRelease");
                currentLoopIndex = 0;
                break;
            case 38: // Up Arrow
                gamerInput = new GamerInput("UpRelease");
                break; 
            case 39: // Right Arrow
                gamerInput = new GamerInput("RightRelease");
                currentLoopIndex = 0;
                break;
            case 40: // Down Arrow
                gamerInput = new GamerInput("DownRelease");
                break;
        }
    }
}

const speed = 3;

var yellowButton = document.getElementsByClassName("yellow")[0];

function update() {
    // Check Input

    yellowButton.classList.remove("active");

    if (gamerInput.action === "Up") {
        //console.log("Move Up");
        player.y -= speed; // Move Player Up
        //Increment Frame
        frameCount++;

       
        yellowButton.classList.add("active");
    } 
    else if (gamerInput.action === "Down") {
        //console.log("Move Down");
        player.y += speed; // Move Player Down
        //Increment Frame
        frameCount++;
    } 
    else if (gamerInput.action === "Left") {
        //console.log("Move Left");
        player.x -= speed; // Move Player Left
        //Increment Frame
        frameCount++;
        currentDirection = 1;
    } 
    else if (gamerInput.action === "Right") {
        //console.log("Move Right");
        player.x += speed; // Move Player Right
        //Increment Frame
        frameCount++;
        currentDirection = 0;
    }  
    else if (gamerInput.action === "E") {
        //console.log("Rotate Right");
        context.rotate(1 * Math.PI/180);
    }
    else if (gamerInput.action === "Q") {
        //console.log("Rotate Left");
        context.rotate(-1 * Math.PI/180);
    }
    else if (gamerInput.action === "Space") {
        //console.log("Spacebar");
        playAudio();
    }
}

function playAudio(){
    let bounce = new Audio('bounce.mp3');
    bounce.play();
}

// Prints out the current date
function printLegible(){
    let date = new Date();
    document.getElementById("the_time").innerHTML = "Date: "+date.getDate()+
    "/"+(date.getMonth()+1)+
    "/"+date.getFullYear()+
    " - Time: "+
    " "+date.getHours()+
    ":"+date.getMinutes()+
    ":"+date.getSeconds();

    context.fillStyle = "#000000";
    context.font = "24px Arial";
    context.fillText("Score: " +playerScore, 10, 30);
}

  
window.addEventListener('keydown', input);
window.addEventListener('keyup', input);


//ANIMATION

//Bird draw
function draw() {
    context.drawImage(player3.spritesheet, 134 + (birdFlapIndex * player3.width), 5, player3.width, player3.height, player3.x , player3.y,  player3.width * 2, player3.height * 2);
}

// Draw a HealthBar on Canvas, can be used to indicate players health
function drawHealthbar() 
{
    var width = 80;
    var height = 10;
    var max = 100;
  
    // Draw the background
    context.fillStyle = "#000000";
    context.fillRect(player.x, player.y - 25, width, height);
  
    // Draw the fill
    context.fillStyle = "#00FF00";
    var fillVal = Math.min(Math.max(playerHealth / max, 0), 1);
    context.fillRect(player.x, player.y - 25, fillVal * width, height);
}

const scale = player.scale;
const width = player.width;
const height = player.height;
const scaledWidth = scale * width;
const scaledHeight = scale * height;

function drawFrame(frameX, frameY, canvasX, canvasY) {
    context.drawImage(player.spritesheet, frameX + (walkLoop[currentLoopIndex] * width), frameY + (currentDirection * (height + directionHeight[currentDirection])), width, height, canvasX, canvasY, scaledWidth, scaledHeight);
}

const walkLoop = [0, 2, 3];
const directionHeight = [0, 19]
let currentLoopIndex = 0;
let birdFlapIndex = 0;
let frameCount = 0;
let birdFrameCount = 0;
let currentDirection = 0;

function gameloop()
{
    //Draw Character
    context.clearRect(0, 0, canvas.width, canvas.height);
    drawFrame(677, 0, player.x, player.y);
    draw();

    //Dino Frame Counter
    if (frameCount > 5) {
        frameCount = 0;
        currentLoopIndex++;
    }
    //Bird Frame Counter
    birdFrameCount++
    if (birdFrameCount > 15) {
        birdFrameCount = 0;
        birdFlapIndex++;
    }

    //Dino Frame Counter Reset
    if (currentLoopIndex >= walkLoop.length) 
    {
        currentLoopIndex = 0;
    }
    //Bird Frame Counter Reset
    if (birdFlapIndex >= 2) 
    {
        birdFlapIndex = 0;
    }

    
    update();
    collisionCheck();
    printLegible();
    drawHealthbar();
    window.requestAnimationFrame(gameloop);
}

function collisionCheck()
{
    if (player.x + player.width > player3.x && player.x + player.width < (player3.x + player3.width * 2) &&
    player.y + player.height > player3.y && player.y + player.height < (player3.y + player3.height * 2))
    {
        console.log("hit");
        playerHealth = playerHealth - 1;

        const stringConvert = parseInt(playerScore, 10);
        playerScore = stringConvert + 1;

        localStorage.setItem("score",playerScore);
    }
}
